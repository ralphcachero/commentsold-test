//
//  CommentSoldUITests.swift
//  CommentSoldUITests
//
//  Created by Ralph Cachero on 9/11/19.
//  Copyright © 2019 rcpixels. All rights reserved.
//

import XCTest

class CommentSoldUITests: XCTestCase {

    override func setUp() {
        let app = XCUIApplication()
        setupSnapshot(app)
        app.launch()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testScreenshots() {
        let app = XCUIApplication()
        XCUIDevice.shared.orientation = .portrait
        
        snapshot("0-Home")
        app.buttons["Login"].tap()
        
        snapshot("1-Product List")
    }

}
