//
//  APIManager.swift
//  CommentSold
//
//  Created by Ralph Cachero on 9/6/19.
//  Copyright © 2019 rcpixels. All rights reserved.
//

import Foundation

class APIManager: NSObject {
    
    let API_ENDPOINT = "https://cscodetest.herokuapp.com/api"
    let username = "betty.iraheta@foomail.org"
    let password = "IwBbzQUzcD"
    
    static let sharedInstance = APIManager()
    
    func getTokenWith(name:String, password: String, _ completionHandler: @escaping (_ result: String?) -> Void) {
        let url = URL(string: "\(API_ENDPOINT)/status")
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        
        let config = URLSessionConfiguration.default
        let userPasswordString = "\(username):\(password)"
        let userPasswordData = userPasswordString.data(using: String.Encoding.utf8)
        let base64EncodedCredential = userPasswordData!.base64EncodedString(options: .init(rawValue: 0))
        let authString = "Basic \(base64EncodedCredential)"
        config.httpAdditionalHeaders = ["Authorization" : authString]
        let session = URLSession(configuration: config)
        let task = session.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil else { return }
            let json = try? JSONSerialization.jsonObject(with: data, options: [])
            guard let jsonDict = json as? [String: Any] else { return }
            guard let token = jsonDict["token"] as? String else {
                print("Token is null")
                return
            }
            completionHandler(token)
        }
        task.resume()
    }
    
    func getProductList(_ completionHandler: @escaping (_ productList: Clothing?, _ error: Error?) -> Void) {
        let url = URL(string: "\(API_ENDPOINT)/products")
        guard let token = Credentials.loadAuthToken() else { return }
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        
        let config = URLSessionConfiguration.default
        let authValue: String = "Bearer \(token)"
        config.httpAdditionalHeaders = ["Authorization": authValue,
                                        "Accept": "application/json"]
        let session = URLSession(configuration: config)
        let task = session.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil else {
                completionHandler(nil, error)
                return
            }
            do {
                let decoder = JSONDecoder()
                let clothingList = try decoder.decode(Clothing.self, from: data)
                completionHandler(clothingList, nil)
            } catch (let err) {
                completionHandler(nil, err)
                print("JSON Decode List Error: \(err)")
            }
        }
        task.resume()
    }
    
    func getProductDetailsWith(id: Int, _ completionHandler: @escaping (_ result: Item.Product?, _ error: Error?) -> Void) {
        let url = URL(string: "\(API_ENDPOINT)/product/\(id)")
        guard let token = Credentials.loadAuthToken() else { return }
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        
        let config = URLSessionConfiguration.default
        let authValue: String = "Bearer \(token)"
        config.httpAdditionalHeaders = ["Authorization": authValue,
                                        "Accept": "application/json"]
        let session = URLSession(configuration: config)
        let task = session.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil else {
                completionHandler(nil, error)
                return
            }
            do {
                let decoder = JSONDecoder()
                let productItem = try decoder.decode(Item.self, from: data)
                completionHandler(productItem.product, nil)
            } catch (let err) {
                completionHandler(nil, err)
                print("JSON Decode List Error: \(err)")
            }
        }
        task.resume()
    }
}

