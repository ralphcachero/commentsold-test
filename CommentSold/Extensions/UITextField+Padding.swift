//
//  UITextField+Padding.swift
//  CommentSold
//
//  Created by Ralph Cachero on 9/5/19.
//  Copyright © 2019 rcpixels. All rights reserved.
//

import UIKit

extension UITextField {
    func setLeftPaddingPoints(_ space: CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: space, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
}
