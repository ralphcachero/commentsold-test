//
//  Credentials.swift
//  CommentSold
//
//  Created by Ralph Cachero on 9/9/19.
//  Copyright © 2019 rcpixels. All rights reserved.
//

import Foundation

class Credentials {
    
    open class func loadEmail() -> String? {
        return KeychainService.loadKey(key: "email") as String?
    }
    
    open class func saveEmail(_ email: String) {
        KeychainService.saveKey("email", val: email)
    }
    
    open class func loadAuthToken() -> String? {
        return KeychainService.loadKey(key: "auth_token") as String?
    }
    
    open class func saveAuthToken(_ auth_token: String) {
        KeychainService.saveKey("auth_token", val: (auth_token as String))
    }
    
    open class func clearAll() {
        KeychainService.clearAll()
    }
    
    open class func firstTimeRun() -> Bool {
        let userDefaults = UserDefaults.standard
        return !userDefaults.bool(forKey: "hasRunBefore")
    }
    
    open class func setAppHasRun() {
        let userDefaults = UserDefaults.standard
        userDefaults.set(true, forKey: "hasRunBefore")
        userDefaults.synchronize()
    }
    
}
