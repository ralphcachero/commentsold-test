//
//  Keychain.swift
//  CommentSold
//
//  Created by Ralph Cachero on 9/9/19.
//  Copyright © 2019 rcpixels. All rights reserved.
//

import Foundation
import Security

// Constant Identifiers
let userAccount = "AuthenticatedUser"

// Arguments for the keychain queries
let kSecClassValue = NSString(format: kSecClass)
let kSecAttrAccountValue = NSString(format: kSecAttrAccount)
let kSecValueDataValue = NSString(format: kSecValueData)
let kSecClassGenericPasswordValue = NSString(format: kSecClassGenericPassword)
let kSecAttrServiceValue = NSString(format: kSecAttrService)
let kSecMatchLimitValue = NSString(format: kSecMatchLimit)
let kSecReturnDataValue = NSString(format: kSecReturnData)
let kSecMatchLimitOneValue = NSString(format: kSecMatchLimitOne)

open class KeychainService: NSObject {
    
    // MARK: - Exposed methods to perform save, load, and delete queries.
    
    open class func saveKey(_ key: String, val: String) {
        print("Saving \(key) into keychain")
        self.save(key as NSString, data: val as NSString)
    }
    
    open class func loadKey(key: String) -> NSString? {
        print("Loading \(key) from keychain")
        return self.load(key as NSString)
    }
    
    @discardableResult open class func removeKey(key: String) -> Bool {
        print("Remove \(key) auth token from keychain")
        return self.remove(key as NSString)
    }
    
    @discardableResult open class func clearAll() -> Bool {
        print("Clearing all keychains")
        return self.clear()
    }
    
    // MARK: - Internal methods for querying the keychain.
    
    fileprivate class func save(_ service: NSString, data: NSString) {
        let dataFromString: Data = data.data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)!
        
        // Instantiate a new default keychain query
        let keychainQuery: NSMutableDictionary = NSMutableDictionary(objects: [kSecClassGenericPasswordValue,
                                                                               service, userAccount, dataFromString],
                                                                     forKeys: [kSecClassValue, kSecAttrServiceValue,
                                                                               kSecAttrAccountValue,
                                                                               kSecValueDataValue])
        // Delete any existing items
        SecItemDelete(keychainQuery as CFDictionary)
        
        // Add the new keychain item
        SecItemAdd(keychainQuery as CFDictionary, nil)
    }
    
    fileprivate class func load(_ service: NSString) -> NSString? {
        let keychainQuery: NSMutableDictionary = NSMutableDictionary(objects: [kSecClassGenericPasswordValue,
                                                                               service, userAccount,
                                                                               kCFBooleanTrue!,
                                                                               kSecMatchLimitOneValue],
                                                                     forKeys: [kSecClassValue, kSecAttrServiceValue, kSecAttrAccountValue, kSecReturnDataValue, kSecMatchLimitValue])
        var dataTypeRef :AnyObject?
        
        // Search for the keychain items
        let status: OSStatus = SecItemCopyMatching(keychainQuery, &dataTypeRef)
        var contentsOfKeychain: NSString? = nil
        
        if status == errSecSuccess {
            if let retrievedData = dataTypeRef as? Data {
                contentsOfKeychain = NSString(data: retrievedData, encoding: String.Encoding.utf8.rawValue)
            }
        }
        
        return contentsOfKeychain
    }
    
    fileprivate class func remove(_ service: NSString) -> Bool {
        let keychainQuery: NSMutableDictionary = NSMutableDictionary(objects: [kSecClassGenericPasswordValue,
                                                                               service, userAccount],
                                                                     forKeys: [kSecClassValue, kSecAttrServiceValue, kSecAttrAccountValue])
        return SecItemDelete(keychainQuery as CFDictionary) == noErr
    }
    
    fileprivate class func clear() -> Bool {
        let keychainQuery: NSMutableDictionary = NSMutableDictionary(objects: [kSecClassGenericPasswordValue, userAccount],
                                                                     forKeys: [kSecClassValue, kSecAttrAccountValue])
        return SecItemDelete(keychainQuery as CFDictionary) == noErr
    }
}
