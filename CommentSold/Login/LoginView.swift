//
//  LoginView.swift
//  CommentSold
//
//  Created by Ralph Cachero on 9/4/19.
//  Copyright © 2019 rcpixels. All rights reserved.
//

import UIKit

class LoginView: UIView {
    
    var loginAction: (() -> Void)?
    
    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init coder has not been implemented")
    }
    
    // MARK: - UI Components
    let backgroundImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "loginbg")
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    let emailTextField: UITextField = {
        let textField = UITextField()
        textField.borderStyle = .none
        textField.layer.cornerRadius = 5
        textField.backgroundColor = UIColor(red: 216/255.0, green: 216/255.0, blue: 216/255.0, alpha: 1)
        textField.textColor = UIColor.black
        textField.font = UIFont.systemFont(ofSize: 17)
        textField.autocorrectionType = .no
        
        var placeholder = NSMutableAttributedString()
        placeholder = NSMutableAttributedString(attributedString: NSAttributedString(string: "Email",
                                                                                     attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18), .foregroundColor: UIColor.black]))
        textField.attributedPlaceholder = placeholder
        textField.setAnchor(width: 0, height: 40)
        textField.setLeftPaddingPoints(20)
        return textField
    }()
    
    let passwordTextField: UITextField = {
        let textField = UITextField()
        textField.borderStyle = .none
        textField.layer.cornerRadius = 5
        textField.backgroundColor = UIColor(red: 216/255.0, green: 216/255.0, blue: 216/255.0, alpha: 1)
        textField.textColor = UIColor.black
        textField.font = UIFont.systemFont(ofSize: 17)
        textField.autocorrectionType = .no
        
        var placeholder = NSMutableAttributedString()
        placeholder = NSMutableAttributedString(attributedString:
            NSAttributedString(string: "Password",
                               attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18),
                                            .foregroundColor: UIColor.black]))
        textField.attributedPlaceholder = placeholder
        textField.setAnchor(width: 0, height: 40)
        textField.setLeftPaddingPoints(20)
        return textField
    }()
    
    let loginButton: UIButton = {
        let button = UIButton()
        let attributedString = NSMutableAttributedString(attributedString:
            NSAttributedString(string: "Login",
                               attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18),
                                            .foregroundColor: UIColor.black]))
        button.setAttributedTitle(attributedString, for: .normal)
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor(red: 80/255, green: 227/255, blue: 194/255, alpha: 1).cgColor
        button.setAnchor(width: 0, height: 50)
        button.addTarget(self, action: #selector(handleLogin), for: .touchUpInside)
        return button
    }()
    
    // MARK: - Setup view
    func setup() {
        let stackView = mainStackView()
        
        addSubview(backgroundImageView)
        addSubview(stackView)
        
        backgroundImageView.setAnchor(top: self.topAnchor, leading: self.leadingAnchor,
                                      trailing: self.trailingAnchor, bottom: self.bottomAnchor,
                                      paddingTop: 0, paddingLeading: 0, paddingTrailing: 0,
                                      paddingBottom: 0)
        stackView.setAnchor(width: self.frame.width - 60, height: 0)
        stackView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        stackView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
    }
    
    func mainStackView() -> UIStackView {
        let stackView = UIStackView(arrangedSubviews: [emailTextField, passwordTextField, loginButton])
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.spacing = 10
        return stackView
    }
    
    // MARK: - Button Actions
    @objc func handleLogin() {
        loginAction?()
    }
}
