//
//  ViewController.swift
//  CommentSold
//
//  Created by Ralph Cachero on 9/4/19.
//  Copyright © 2019 rcpixels. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    var loginView: LoginView!
    var username = "betty.iraheta@foomail.org"
    var password = "IwBbzQUzcD"
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    func setupView() {
        let mainView = LoginView(frame: self.view.frame)
        self.loginView = mainView
        self.loginView.loginAction = loginPressed
        self.view.addSubview(loginView)
        loginView.setAnchor(top: view.topAnchor, leading: view.leadingAnchor,
                            trailing: view.trailingAnchor, bottom: view.bottomAnchor,
                            paddingTop: 0, paddingLeading: 0, paddingTrailing: 0, paddingBottom: 0)
    }
    
    func loginPressed() {
        print("login button pressed")
        checkIfTextFieldsEmpty()
        APIManager.sharedInstance.getTokenWith(name: username, password: password) { (token) in
            DispatchQueue.main.async {
                if let token = token {
                    Credentials.saveAuthToken(token)
                    self.navigationController?.pushViewController(ProductViewController(), animated: true)
                } else {
                    print("Unable to login")
                }
            }
        }
    }
    
    func checkIfTextFieldsEmpty() {
        if let emailText = loginView.emailTextField.text,
            let passwordText = loginView.passwordTextField.text {
            if (emailText.count > 0) {
                username = emailText
            }
            if (passwordText.count > 0) {
                password = passwordText
            }
        }
        
    }
}

