//
//  Products.swift
//  CommentSold
//
//  Created by Ralph Cachero on 9/9/19.
//  Copyright © 2019 rcpixels. All rights reserved.
//

import UIKit

struct Clothing: Codable {
    var total: Int
    let products: [Item.Product]
}

struct Item: Codable { 
    let product: Product
    struct Product: Codable {
        let brand: String
        let description: String
        let id: Int
        let note: String
        let product_name: String
        let product_type: String
        let shipping_price: Int
        let style: String
        let url: String
    }
}

