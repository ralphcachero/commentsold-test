//
//  ProductDetailsViewController.swift
//  CommentSold
//
//  Created by Ralph Cachero on 9/11/19.
//  Copyright © 2019 rcpixels. All rights reserved.
//

import UIKit

class ProductDetailsViewController: UIViewController {
    
    var id: Int = 0
    var productDetailsView: ProductDetailsView!
    
    override func loadView() {
        productDetailsView = ProductDetailsView()
        self.view = productDetailsView
        getDetails()
    }
    
    func setupView() {
        self.productDetailsView = ProductDetailsView(frame: self.view.frame)
        self.view.addSubview(productDetailsView)
        productDetailsView.setAnchor(top: view.topAnchor, leading: view.leadingAnchor,
                            trailing: view.trailingAnchor, bottom: view.bottomAnchor,
                            paddingTop: 0, paddingLeading: 0, paddingTrailing: 0, paddingBottom: 0)
    }
    
    func getDetails() {
        APIManager.sharedInstance.getProductDetailsWith(id: id) { (product, error) in
            DispatchQueue.main.async {
                if let item = product {
                    self.productDetailsView.updateLabelsWith(item: item)
                    self.title = item.product_name
                }
            }
        }
    }
    
}
