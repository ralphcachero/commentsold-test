//
//  ProductDetailsView.swift
//  CommentSold
//
//  Created by Ralph Cachero on 9/11/19.
//  Copyright © 2019 rcpixels. All rights reserved.
//

import UIKit

class ProductDetailsView: UIView {
    
    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init coder has not been implemented")
    }
    
    // MARK: - Labels Components
    let productNameLabel: UILabel = {
        let name = UILabel()
        name.text = "Product Name:"
        name.textColor = UIColor.black
        name.font = UIFont.systemFont(ofSize: 17)
        name.setAnchor(width: 0, height: 40)
        return name
    }()
    
    let brandLabel: UILabel = {
        let name = UILabel()
        name.text = "Brand:"
        name.textColor = UIColor.black
        name.font = UIFont.systemFont(ofSize: 17)
        name.setAnchor(width: 0, height: 40)
        return name
    } ()
    
    // MARK: - Name Components
    let productName: UILabel = {
        let name = UILabel()
        name.text = "Product"
        name.textColor = UIColor.black
        name.font = UIFont.systemFont(ofSize: 17)
        name.setAnchor(width: 0, height: 40)
        return name
    }()
    
    let brandName: UILabel = {
        let name = UILabel()
        name.text = "Brand"
        name.textColor = UIColor.black
        name.font = UIFont.systemFont(ofSize: 17)
        name.setAnchor(width: 0, height: 40)
        return name
    } ()
    
    // MARK: - Setup view
    func setup() {
        self.backgroundColor = .white
        let mainView = mainStackView()
        addSubview(mainView)
        
        mainView.setAnchor(width: self.frame.width, height: 0)
        mainView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        mainView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
    }
    
    // MARK: - Stack Views
    func mainStackView() -> UIStackView {
        let stackView = UIStackView(arrangedSubviews: [productNameStackView(), brandStackView()])
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.spacing = 10
        return stackView
    }
    
    func productNameStackView() -> UIStackView {
        let stackView = UIStackView(arrangedSubviews: [productNameLabel, productName])
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.spacing = 5
        return stackView
    }
    
    func brandStackView() -> UIStackView {
        let stackView = UIStackView(arrangedSubviews: [brandLabel, brandName])
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.spacing = 5
        return stackView
    }
    
    func updateLabelsWith(item: Item.Product) {
        productName.text = item.product_name
        brandName.text = item.brand
    }
}
