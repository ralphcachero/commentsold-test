//
//  ProductViewController.swift
//  CommentSold
//
//  Created by Ralph Cachero on 9/9/19.
//  Copyright © 2019 rcpixels. All rights reserved.
//

import UIKit

class ProductViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    let total: Int = 0
    var products: [Item.Product] = []
    let tableView = UITableView()
    
    // MARK: - Init
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .red
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = false
        self.title = "Products"
        
        APIManager.sharedInstance.getProductList { (result, err)  in
            DispatchQueue.main.async {
                if let clothing = result {
                    self.products = clothing.products
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        view.addSubview(tableView)
        tableView.setAnchor(top: view.topAnchor, leading: view.leadingAnchor,
                            trailing: view.trailingAnchor, bottom: view.bottomAnchor,
                            paddingTop: 0, paddingLeading: 0,
                            paddingTrailing: 0, paddingBottom: 0)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "productCell")
    }
}

extension ProductViewController {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: "productCell")
        cell.textLabel?.text = products[indexPath.row].product_name
        cell.detailTextLabel?.text = products[indexPath.row].brand
        cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedItem = products[indexPath.row]
        let productDetailsVC = ProductDetailsViewController()
        productDetailsVC.id = selectedItem.id
        self.navigationController?.pushViewController(productDetailsVC, animated: true)
    }
}
